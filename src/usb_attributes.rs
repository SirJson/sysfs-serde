use crate::uevent::UEvent;
#[cfg(feature = "serde")]
use serde::Serialize;
use std::collections::HashMap;
use std::str::FromStr;

/// De-serialized /sys/class/bus/usb to a Rust structure.
/// Note unknown fields are stored in raw format in the attributes: HashMap<String, String>
#[cfg_attr(feature = "serde", derive(Serialize))]
#[derive(Default)]
pub struct UsbAttributes {
    pub num_endpoints: u8,
    pub num_configurations: u8,
    pub num_interfaces: u8,
    pub interface_class: u8,
    pub interface_number: u8,
    pub interface_protocol: u8,
    pub alternate_setting: u8,
    pub interface_subclass: u8,
    pub max_packet_size0: u16,
    pub supports_autosuspend: u8,
    pub authorized: u8,
    #[cfg_attr(feature = "serde", serde(skip_serializing_if = "String::is_empty"))]
    pub modalias: String,
    #[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
    pub id_product: Option<String>,
    #[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
    pub id_vendor: Option<String>,
    pub bus_num: u8,
    pub dev_num: u8,
    #[cfg_attr(feature = "serde", serde(skip_serializing_if = "String::is_empty"))]
    pub product: String,
    #[cfg_attr(feature = "serde", serde(skip_serializing_if = "String::is_empty"))]
    pub manufacturer: String,
    #[cfg_attr(feature = "serde", serde(skip_serializing_if = "String::is_empty"))]
    pub serial: String,
    #[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
    pub uevent: Option<UEvent>,
    #[cfg_attr(feature = "serde", serde(skip))]
    pub descriptors: Vec<u8>,
    #[cfg_attr(
        feature = "serde",
        serde(flatten, skip_serializing_if = "HashMap::is_empty")
    )]
    pub attributes: HashMap<String, String>,
}

impl UsbAttributes {
    pub(crate) fn add(&mut self, key: &str, value: Vec<u8>) -> Result<(), std::num::ParseIntError> {
        if key == "descriptors" {
            // Raw descriptors from the device is stored in a Vec.
            self.descriptors = value;
            return Ok(());
        }

        let value = if let Ok(value) = String::from_utf8(value) {
            value.trim().to_string()
        } else {
            log::warn!("Could not decode {} as UTF8 string.", key);
            return Ok(());
        };
        match &key[0..] {
            "busnum" => self.bus_num = value.parse()?,
            "devnum" => self.dev_num = value.parse()?,
            "supports_autosuspend" => self.supports_autosuspend = value.parse()?,
            "authorized" => self.authorized = value.parse()?,
            "uevent" => self.uevent = UEvent::from_str(&value).ok(),
            "bNumEndpoints" => self.num_endpoints = value.parse()?,
            "bNumInterfaces" => self.num_interfaces = value.parse()?,
            "bNumConfigurations" => self.num_configurations = value.parse()?,
            "bInterfaceNumber" => self.interface_number = u8::from_str_radix(&value, 16)?,
            "bInterfaceProtocol" => self.interface_protocol = u8::from_str_radix(&value, 16)?,
            "bInterfaceClass" => self.interface_class = u8::from_str_radix(&value, 16)?,
            "bInterfaceSubClass" => self.interface_subclass = u8::from_str_radix(&value, 16)?,
            "bAlternateSetting" => self.alternate_setting = u8::from_str_radix(&value, 16)?,
            "bMaxPacketSize0" => self.max_packet_size0 = value.parse()?,
            "manufacturer" => self.manufacturer = value,
            "product" => self.product = value,
            "serial" => self.serial = value,
            _ => {
                self.attributes.insert(key.into(), value);
            }
        }

        Ok(())
    }
}
