#[cfg(feature = "serde")]
use serde::Serialize;
use std::collections::HashMap;
use std::str::FromStr;
#[cfg_attr(feature = "serde", derive(Serialize))]
#[derive(Default)]
pub struct UEvent {
    attributes: HashMap<String, String>,
}

impl FromStr for UEvent {
    type Err = std::io::Error;
    fn from_str(lines: &str) -> Result<Self, std::io::Error> {
        let mut uevent = UEvent::default();
        for line in lines.lines() {
            let mut kv = line.split('=');
            if let Some(key) = kv.next() {
                uevent
                    .attributes
                    .insert(key.into(), kv.next().unwrap_or("").into());
            }
        }
        if uevent.attributes.is_empty() {
            return Err(std::io::Error::from_raw_os_error(0));
        }
        Ok(uevent)
    }
}

impl UEvent {
    pub fn value(&self, key: &str) -> Option<&String> {
        self.attributes.get(key)
    }
}
