use crate::uevent::UEvent;
#[cfg(feature = "serde")]
use serde::Serialize;
use std::collections::HashMap;
use std::str::FromStr;

#[cfg_attr(feature = "serde", derive(Serialize))]
#[derive(Default)]
pub struct PciAttributes {
    pub class: u32,
    pub subsystem_vendor: String,
    pub subsystem_device: String,
    pub dma_mask_bits: u8,
    pub vendor: String,
    pub device: String,
    pub enable: u8,
    pub revision: u16,
    pub irq: u8,
    pub local_cpus: u64, // As defined by the Nix crate
    #[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
    pub uevent: Option<UEvent>,
    pub resource: Vec<u64>,
    pub ari_enabled: u8,
    pub current_link_width: u8,
    pub max_link_width: u8,
    pub msi_bus: u8,
    pub consistent_dma_mask_bits: u8,
    pub secondary_bus_number: u8,
    pub subordinate_bus_number: u8,
    pub broken_parity_status: u8,
    pub d3cold_allowed: u8,
    pub numa_node: i8,
    pub modalias: String,
    pub current_link_speed: String,
    pub max_link_speed: String,
    pub driver_override: Option<String>,
    pub local_cpulist: String,
    pub aer_dev_fatal: String,
    pub aer_dev_nonfatal: String,
    pub aer_dev_correctable: String,
    #[cfg_attr(feature = "serde", serde(skip))]
    pub config: Vec<u8>,
    #[cfg_attr(
        feature = "serde",
        serde(flatten, skip_serializing_if = "HashMap::is_empty")
    )]
    pub attributes: HashMap<String, String>,
}

impl PciAttributes {
    pub(crate) fn add(&mut self, key: &str, value: Vec<u8>) -> Result<(), std::num::ParseIntError> {
        if key == "config" {
            self.config = value;
            return Ok(());
        }
        let value = String::from_utf8_lossy(&value)
            .to_string()
            .trim()
            .to_string();
        match &key[0..] {
            "revision" => {
                self.revision = u16::from_str_radix(&value[2..], 16)?;
            }
            "uevent" => {
                self.uevent = UEvent::from_str(&value).ok();
            }
            "enable" => self.enable = value.parse()?,
            "class" => self.class = u32::from_str_radix(&value[2..], 32)?,
            "numa_node" => self.numa_node = value.parse()?,
            "dma_mask_bits" => self.dma_mask_bits = value.parse()?,
            "current_link_width" => self.current_link_width = value.parse()?,
            "max_link_width" => self.max_link_width = value.parse()?,
            "consistent_dma_mask_bits" => self.consistent_dma_mask_bits = value.parse()?,
            "secondary_bus_number" => self.secondary_bus_number = value.parse()?,
            "subordinate_bus_number" => self.subordinate_bus_number = value.parse()?,
            "d3cold_allowed" => self.d3cold_allowed = value.parse()?,
            "driver_override" => {
                if value != "(null)" {
                    self.driver_override = Some(value);
                }
            }
            "broken_parity_status" => self.broken_parity_status = value.parse()?,
            "local_cpus" => {
                self.local_cpus = u64::from_str_radix(&value, 16)?;
            }
            "msi_bus" => self.msi_bus = value.parse()?,
            "ari_enabled" => self.ari_enabled = value.parse()?,
            "irq" => self.irq = value.parse()?,
            "resource" => {
                let value = value.replace('\n', " ");
                for r in value.split(' ') {
                    self.resource.push(u64::from_str_radix(&r[2..], 16)?);
                }
            }
            _ => {
                self.attributes.insert(key.into(), value);
            }
        }
        Ok(())
    }
}
