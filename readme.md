# sysfs-serde

This crate try to Deserialize SysFs device path's to Rust data types.
The Rust data types can then be serialized to JSON, TOML, YAML etc... 

Note that unknown fields will be stored in raw format in a ```HashMap<String, String>```.

This crate can be used to lookup USB/PCI devices.

For example this crate is currently used by usbapi-rs to find USB devices on a Linux Host.

See a documentation and example how to use this crate.

# Supported platform

 - Linux

Feel free to add other OS'es.

# Supported sysfs paths

See SysFsType and documentation what path's is supported.

# Dependencies

 - log
 - serde (features = "serde")
