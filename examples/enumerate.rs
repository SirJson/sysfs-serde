use sysfs_serde::SysFs;
fn main() {
    env_logger::init();
    let p = SysFs::usb_devices().unwrap();
    println!("{}", serde_json::to_string_pretty(&p).unwrap());
    let p = SysFs::pci_devices().unwrap();
    println!("{}", serde_json::to_string_pretty(&p).unwrap());
    let p = SysFs::dmi_info().unwrap();
    println!("{}", serde_json::to_string_pretty(&p).unwrap());
    let p = SysFs::thermal_info().unwrap();
    println!("{}", serde_json::to_string_pretty(&p).unwrap());
}
